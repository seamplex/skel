/*------------ -------------- -------- --- ----- ---   --       -            -
 *  skel's version banner
 *
 *  Copyright (C) 2015--2017 jeremy theler
 *
 *  This file is part of skel.
 *
 *  skel is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  skel is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */

#include <stdio.h>

#include "skel.h"
#include "version.h"

// global static so the compiler locates these strings in the text section
// so when the plugin_* functions return pointers to the strings there is
// no need to free them afterward
const char skelname[] = "skel";
const char skeldescription[] = "dummy plugin skeleton for wasora";
char skelshortversion[128];
char skellongversion[2048];
const char skelusage[] = "no commandline options needed";

const char skelcopyright[] = "\
 skel is copyright (c) 2015--2017 jeremy theler\n\
 licensed under GNU GPL version 3 or later.\n\
 skel is free software: you are free to change and redistribute it.\n\
 There is NO WARRANTY, to the extent permitted by law.";

const char skelhmd5[] = PLUGIN_HEADERMD5;


const char *plugin_name(void) {
  return skelname;
}

const char *plugin_longversion(void) {
  
#ifdef PLUGIN_VCS_BRANCH
  sprintf(skellongversion, "\n\
 last commit on %s\n\
 compiled on %s by %s@%s (%s)\n\
 with %s using %s\n",
   PLUGIN_VCS_DATE,
   COMPILATION_DATE,
   COMPILATION_USERNAME,
   COMPILATION_HOSTNAME,
   COMPILATION_ARCH,
   CCOMPILER_VERSION,
   CCOMPILER_FLAGS);
#endif

  return skellongversion;
}

const char *plugin_wasorahmd5(void) {
  return skelhmd5;
}
const char *plugin_copyright(void) {
  return skelcopyright;
}


const char *plugin_version(void) {
#ifdef PLUGIN_VCS_BRANCH
  sprintf(skelshortversion, "%s%s %s", PLUGIN_VCS_VERSION,
                                           (PLUGIN_VCS_CLEAN==0)?"":"+Δ",
                                           strcmp(PLUGIN_VCS_BRANCH, "master")?PLUGIN_VCS_BRANCH:"");
#else
  sprintf(skelshortversion, "%s", PACKAGE_VERSION);
#endif
  
  return skelshortversion;
}

const char *plugin_description(void) {
  return skeldescription;
}

const char *plugin_usage(void) {
  return skelusage;
}
