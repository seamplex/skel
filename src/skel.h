/*------------ -------------- -------- --- ----- ---   --       -            -
 *  skel main header
 *
 *  Copyright (C) 2015 jeremy theler
 *
 *  This file is part of skel.
 *
 *  skel is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  skel is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */

#include <wasora.h>

struct {
  
  struct {
    var_t *solution;
  } vars;

} skel;


// skel.c
extern int skel_instruction_step(void *);

// init.c
extern int plugin_init_before_parser(void);
extern int plugin_init_after_parser(void);
extern int plugin_init_before_run(void);
extern int plugin_finalize(void);

// parser.c
extern int skel_parse_line(char *);
extern int skel_define_result_functions(void);

// version.c
extern void skel_usage(char *);
extern void skel_version(FILE *, int, int);
extern void skel_license(FILE *);

extern const char *plugin_name(void);
extern const char *plugin_version(void);
extern const char *plugin_description(void);
extern const char *plugin_longversion(void);
extern const char *plugin_copyright(void);
