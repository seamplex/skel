/*------------ -------------- -------- --- ----- ---   --       -            -
 *  skel's parsing routines
 *
 *  Copyright (C) 2015 jeremy theler
 *
 *  This file is part of skel.
 *
 *  skel is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  skel is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */
#include "skel.h"

int plugin_parse_line(char *line) {
  
  char *token;

  if ((token = wasora_get_next_token(line)) != NULL) {

// --- SKEL_STEP ------------------------------------------------------
    if (strcasecmp(token, "SKEL_STEP") == 0) {

      wasora_define_instruction(skel_instruction_step, NULL);
      return WASORA_PARSER_OK;

    }
  }

  return WASORA_PARSER_UNHANDLED;
  
  
}

