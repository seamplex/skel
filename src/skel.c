/*------------ -------------- -------- --- ----- ---   --       -            -
 *  skel plugin for wasora
 *
 *  Copyright (C) 2015 jeremy theler
 *
 *  This file is part of skel.
 *
 *  skel is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  skel is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with wasora.  If not, see <http://www.gnu.org/licenses/>.
 *------------------- ------------  ----    --------  --     -       -         -
 */

#include "skel.h"


int skel_instruction_step(void *arg) {

  double t;
  
  t = wasora_value(wasora_special_var(t));
  
  wasora_var(skel.vars.solution) = 0.5 * t*t;
  return WASORA_RUNTIME_OK;
  
}


