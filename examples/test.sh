#!/bin/sh

input=test.was

if [ -e ${input} ]; then
 testdir=""
elif [ -e examples/${input} ]; then
 testdir="examples/"
else
 echo wrong PWD
 exit 1
fi

if [ -e ./skel ]; then
 skelbin=./skel
elif [ -e ../skel ]; then
 skelbin=../skel
elif [ ! -z "`which wasora`" ]; then
 skelbin="wasora -p skel"
else
 echo "do not know how to run skel :("
 exit 1
fi

${skelbin} ${testdir}${input}
outcome=$?

exit $outcome
